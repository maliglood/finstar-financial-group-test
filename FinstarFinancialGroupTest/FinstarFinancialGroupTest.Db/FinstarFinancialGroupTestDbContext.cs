﻿using FinstarFinancialGroupTest.Db.Models;
using Microsoft.EntityFrameworkCore;

namespace FinstarFinancialGroupTest.Db
{
    public class FinstarFinancialGroupTestDbContext : DbContext
    {
        public DbSet<CodeValueModel> CodeValueModels { get; set; }

        public FinstarFinancialGroupTestDbContext(DbContextOptions<FinstarFinancialGroupTestDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var modelsBuilder = builder.Entity<CodeValueModel>();

            modelsBuilder.HasKey(m => m.Code);

            modelsBuilder
                .HasIndex(m => m.IndexNumber)
                .IsUnique();
        }
    }
}
