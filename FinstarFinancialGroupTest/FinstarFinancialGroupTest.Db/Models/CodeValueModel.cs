﻿using System.ComponentModel.DataAnnotations;

namespace FinstarFinancialGroupTest.Db.Models
{
    public class CodeValueModel
    {
        public int IndexNumber { get; set; }

        [Key]
        public int Code { get; set; }

        public string Value { get; set; }
    }
}
