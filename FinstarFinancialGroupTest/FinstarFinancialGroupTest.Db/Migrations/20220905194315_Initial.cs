﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FinstarFinancialGroupTest.Db.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CodeValueModels",
                columns: table => new
                {
                    Code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IndexNumber = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodeValueModels", x => x.Code);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CodeValueModels_IndexNumber",
                table: "CodeValueModels",
                column: "IndexNumber",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CodeValueModels");
        }
    }
}
